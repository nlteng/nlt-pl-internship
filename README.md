# Welcome to the NoLedge Tech internship program! 

This is the recruitment assignment for you.

Your goal is to implement a simple translator. You will get 2 paths to the dictionary files: `PLtoEN.dsv` and `ENtoIT.dsv`, with `|` as a delimiter

1) Implement `generateDictionary` function taking 2 file paths, generating a third file, containing Polish to Italian translation, and returning its path.

2) Implement unit tests.

3) Provide short summary about how to compile the code, run the application, execute the test cases.

You can use the language of your choice (though review of languages other than Scala, Kotlin, Java, Python or Go will take more time). 
Please consider the code style, performance of your method and accuracy of the test cases.

### Example:
```
/Users/test/internship/dictionaries/PLtoEN.dsv

piłka|ball
słońce|sun
```

```
/Users/test/internship/dictionaries/ENtoIT.dsv

ball|palla
sun|sole
```

```
function generateDictionary(plToEnPath, enToItPath) {
  // the logic
  return plToItPath;
}
```

will return the path of created file, ex. `/Users/test/internship/dictionaries/output.dsv` and create
```
/Users/test/internship/dictionaries/output.dsv

piłka|palla
słońce|sole

```

### How to apply?
Please add the read access to your private bitbucket repository for: `pniemiec@noledgetech.com`, `mslapinski@noledgetech.com` 
and send a link to your repo with your CV to `internship@noledgetech.com`


For more details see [the internship page](https://noledgetech.com/internship) 
